import React from 'react'
import Calculadora from './components/Calculadora/Calculadora.jsx'
import './App.css';

function App() {
  return (
    <div id="app">
        <Calculadora></Calculadora>
    </div>
  );
}

export default App;
