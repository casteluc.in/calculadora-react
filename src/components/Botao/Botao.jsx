import React from 'react'
import './Botao.css'

function Botao(props) {
    let classes = ""
    classes += props.duplo ? "duplo ":""
    classes += props.triplo ? "triplo ":""
    classes += props.colorido ? "colorido":""

    return(
        <button className={classes} onClick={e => props.click && props.click(props.rotulo)}>
            {props.rotulo}
        </button>
    )
}

export default Botao