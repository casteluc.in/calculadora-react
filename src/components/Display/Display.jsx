import React from 'react'
import './Display.css'

export default function Display({valor}) {
    return (
        <div className="display">
            {valor.slice(0, 14)}
        </div>
    )
}
