import React, { useState } from 'react'
import Botao from '../Botao/Botao.jsx'
import Display from '../Display/Display.jsx'

import './Calculadora.css'

function Calculadora() {
    const [valorAtual, setValorAtual] = useState('0')
    const [valorAntigo, setValorAntigo] = useState('0')
    const [operador, setOperador] = useState(null)
    const [podeLimpar, setPodeLimapar] = useState(false)

    const limparDisplay = () => {
        setValorAtual('0')
        setValorAntigo('0')
        setOperador(null)
        setPodeLimapar(false)
    }

    const adicionarDigito = digito => {
        if ((valorAtual.includes('.') && digito === '.') ||
            (valorAtual === '0' && digito === '.')) {
            return
        } 
        
        if (valorAtual === '0' && digito === '0') {
            return
        } else if ((valorAtual === '0' && digito !== '0') || podeLimpar) {
            setValorAtual(digito)
            setPodeLimapar(false)
        } else {
            setValorAtual(valorAtual + digito)
        }
    }

    const adicionarOperador = op => {
        if (operador !== null) {
            calculaResultado()
        }
        setPodeLimapar(true)
        setValorAntigo(valorAtual)
        setOperador(op)
    }

    const calculaResultado = () => {
        if (operador != null) {
            let resultado = eval(valorAntigo + operador + valorAtual)
            setOperador(null)
            setValorAtual(resultado.toString()) 
        }
    }

    return(
        <div id="calculadora">
            <Display valor={valorAtual}/>
            <Botao rotulo="AC" click={limparDisplay} triplo/>
            <Botao rotulo="/" click={adicionarOperador} colorido/>
            <Botao rotulo="7" click={adicionarDigito}/>
            <Botao rotulo="8" click={adicionarDigito}/>
            <Botao rotulo="9" click={adicionarDigito}/>
            <Botao rotulo="*" click={adicionarOperador} colorido/>
            <Botao rotulo="4" click={adicionarDigito}/>
            <Botao rotulo="5" click={adicionarDigito}/>
            <Botao rotulo="6" click={adicionarDigito}/>
            <Botao rotulo="-" click={adicionarOperador} colorido/>
            <Botao rotulo="1" click={adicionarDigito}/>
            <Botao rotulo="2" click={adicionarDigito}/>
            <Botao rotulo="3" click={adicionarDigito}/>
            <Botao rotulo="+" click={adicionarOperador} colorido/>
            <Botao rotulo="0" click={adicionarDigito} duplo/>
            <Botao rotulo="." click={adicionarDigito}/>
            <Botao rotulo="=" click={calculaResultado} colorido/>
        </div>
    )
}

export default Calculadora